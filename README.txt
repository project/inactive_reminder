=== Inactive Reminder ===

This module gives the user a dropdown box with the interval selection on their
profile edit form and during the registration. At the moment there is no configuration
screen, so you'll have to modify the inactive_reminder.module file directly to
make changes.

If there is sufficient interest, I will continue the development and make this module
more flexible.

For now it is presented as is.

Drupal contact: mr.andrey
